require('dotenv').config();
const express = require('express'); // import
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000; // puerto puede ser segun la variable o por 3000
const URL_BASE = '/techu/v1/';
const usersFile = require('./user.json');

app.listen(port, function()
{
  console.log('Node JS escuchando en el puerto ' + port);
});


app.use(body_parser.json());

//Operaciòn get
app.get (URL_BASE + 'holamundo',
  function(request,response){
    console.log('Hola Mundo');
    console.log('Arriba Alianza');
    response.status(200).send({usersFile});
});

//Peticiòn GET a un ùnico Usuario ID (Instanacia) busqueda por ID
  app.get (URL_BASE + 'users/:id',
    function(request,response){
      console.log(request.params.id);
        let pos = request.params.id -1;
        let respuesta =(usersFile[pos] == undefined) ? {"msg": "user no existe"} : usersFile[pos];
        let status_code =(usersFile[pos] == undefined) ? 404 : 200;
          response.status(status_code).send(respuesta);
});

//GET con Query String
  app.get (URL_BASE + 'usersq',
    function(req,res){
      console.log(req.query.id);
      console.log(req.query.country);
  res.send({"msg":"GET con Query String OK"});
});

//Peticiòn POST a usersFile (creaciòn), no olvidar colocar data en el RAW
  app.post (URL_BASE + 'users',
    function(req,res){
        console.log ('POST users');
          let tam = usersFile.length;
            let new_user = {
              "id_user" : tam +1,
              "first_name":req.body.first_name,
              "last_name" :req.body.last_name,
              "email" :req.body.email,
              "password" :req.body.password
          }
          console.log(new_user);
            usersFile.push(new_user);
            res.status(201).send({"msg":"Usuario Creado Correctamente" });
});

//Peticiòn PUT a usersFile (actualizaciòn)
    app.put (URL_BASE + 'users/:id',
      function(req,res){
          console.log ('UPDATE users');
            let pos = req.params.id - 1;
              let update_user = {
                "id_user" : pos +1,
                "first_name":req.body.first_name,
                "last_name" :req.body.last_name,
                "email" :req.body.email,
                "password" :req.body.password
            }
            console.log(update_user);
              usersFile[pos] = update_user;
      res.status(200).send({"msg":"Usuario (" + req.params.id + ") Actualizado Correctamente" });
});

//peticiòn PUT a users otra opciòn
    app.put(URL_BASE + 'usersUpdate/:id',
      function (req,res){
          let idBuscar = req.params.id;
            let updateUser = req.body;
              for (i=0; i< usersFile.length;i++){
          console.log(usersFile[i].id);
              if(usersFile[i].id == idBuscar){
                usersFile[i] = updateUser;
    res.status(299).send({"msg" : "usuario actualizado correctamente", updateUser});
          }
        }
    res.status(404).send({"msg" : "usuario no encontrado", updateUser});
});

// Peticiòn Delete A USE (mediante su ID)
      app.delete(URL_BASE + 'users/:id',
          function (req,res){
            console.log('DELETE users');
              let pos = req.params.id - 1;
                usersFile.splice(pos,1);
        res.status(200).send({"msg":"Usuario (" + req.params.id + ") Eliminado Correctamente" });
});

  // LOGIN - users.json
  app.post(URL_BASE + 'login',
    function(request, response) {
      console.log("Prueba de loggin");
      console.log(request.body.email);
      console.log(request.body.password);
        var user = request.body.email;
          var pass = request.body.password;
            for(us of usersFile) {
              if(us.email == user) {
              if(us.password == pass) {
                 us.logged = true;
      writeUserDataToFile(usersFile);
        console.log("Login correcto!");
  response.status(201).send({"msg" : "Login correcto.", "idUsuario" : us.id_user, "logged" : "true"});
              } else {
        console.log("Login incorrecto.");
  response.status(404).send({"msg" : "Login incorrecto."});
          }
        }
      }
  });

  // LOGOUT - users.json
  app.post(URL_BASE + 'logout',
    function(request, response) {
        console.log("Probando logout");
          var userId = request.body.id_user;
            for(us of usersFile) {
              if(us.id == userId) {
              if(us.logged) {
                delete us.logged;
      writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
  response.status(201).send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
                } else {
          console.log("Logout incorrecto.");
  response.status(404).send({"msg" : "Logout incorrecto."});
          }
        }       us.logged = true
      }
  });

//Total users
  app.get(URL_BASE + 'Total_users',
    function(request,response){
      Total_users = usersFile.length;
      response.status(200).send({"Numero Total de Usuarios": Total_users});
});

  function writeUserDataToFile(data) {
      var fs = require('fs');
      var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
 function(err) {
        if(err) {
     console.log(err);
        } else {
     console.log("Datos escritos en 'users.json'.");
       }
}); }
